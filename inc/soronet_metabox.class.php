<?php
class soronet_metabox{
	private $args;

	function __construct($id,$name,$context = 'advanced',$priority = 'high', $screens_to_show = array(),$callback){
		$this->args = array(
			'id' => $id,
			'name' => $name,
			'context' =>$context,
			'priority' => $priority,
			'screens' => $screens_to_show,
			'callback' => $callback
		);
		add_action('add_meta_boxes',array($this,'add_metabox'));
	}
	function add_metabox(){
		foreach($this->args['screens'] as $screen){
			add_meta_box($this->args['id'],$this->args['name'],array($this,'callback'),$screen,$this->args['context'],$this->args['priority']);
		}
	}
	function callback($post){
		global $soronet_custom_settings;
		$cur_template = get_post_meta($post->ID,'_wp_page_template',true);
		wp_nonce_field( basename( __FILE__ ) .'-custom-settings', 'soronet_nonce' );

		echo '<div class="wrap">';
		echo '<input type="hidden" name="full_edit_mode" value="true" />';
		if(sizeof($soronet_custom_settings) < 1){
			echo '<p class="clear">'.__('No settings have been defined','soronet').'</p>';
		}else{
		foreach($soronet_custom_settings as $key => $value){
			if($this->args['id'] != $value['metabox']) continue;
			if(!in_array(get_current_post_type(),$value['supported_post_types'])) continue;
			if(sizeof($value['supported_templates']) > 0 && !in_array($cur_template,$value['supported_templates'])) continue;
			?>
			<p>
				<label style="display: inline-block; width: 200px;" for="<?php echo KEY_PREFIX . $key; ?>" ><?php echo $value['name']; ?></label>
				<?php echo soronet_display_setting($value['type'],$key,$post->ID); ?>
			</p>
		<?php }
		}
		echo '</div>';
	}
}