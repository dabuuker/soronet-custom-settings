### 1) Install like any other Wordpress plugin

### 2) Create a metabox in your functions.php file, for example:

	function theme_setup(){
		soronet_add_metabox('metabox-id','Metabox Title');
	}
	add_action('init','theme_setup');

### 3) Add some settings for your metabox:

	function theme_setup(){
		soronet_add_metabox('metabox-id','Metabox Title');
		soronet_add_setting('metabox-id','setting-id','Setting Title',$type);
	}
	add_action('init','theme_setup');
	
	supported types: 'checkbox','textfield','textarea','image'

### 4) Use soronet_get_setting($post_id,'metabox_id','setting_id') in a php template file:

	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
		<?php if(soronet_get_setting(get_the_ID(),'post_settings','show-title') == 'yes') the_title('<h3>','</h3>');  ?>
    		<?php the_content(); ?>
	<?php endwhile; endif; ?>

### Full function parameters:

	soronet_add_metabox($id,$title,$context = 'advanced',$priority = 'high',$screens_to_show = array());

	soronet_add_setting($metabox_id,$setting_id,$setting_title,$type,$supported_post_types = array(), $supported_templates = array());

	soronet_get_setting($post_id,$metabox,$key,$size = 'thumbnail'); $size is used if setting type is 'image'

	soronet_add_support_for_post_type($post_type); Adds default post type support, 'page' and 'post' already set.
	
