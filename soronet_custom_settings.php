<?php
/*
Plugin Name: Soronet Custom Settings
Description: Add custom settings fields to post types for use in themes
Author: Tanel Kollamaa (Soronet Interactive OÜ)
*/
include('inc/soronet_metabox.class.php');

define("KEY_PREFIX","_soronet-");
$supported_setting_types = array('checkbox','image','textfield','textarea','editor');
$soronet_custom_settings = array();
$screens = array('post','page');
$soronet_boxes = array();
$editors = array();

function soronet_add_metabox($id,$name,$context = 'advanced',$priority = 'high', $screens_to_show = array()){
	global $screens;

	if(sizeof($screens_to_show) < 1) $screens_to_show = $screens;

	new soronet_metabox($id,$name,$context,$priority,$screens_to_show,'soronet_render_metabox');
}

function soronet_add_support_for_post_type($post_type){
    global $screens;
    array_push($screens, $post_type);
}

function soronet_save_custom_settings($post_id){
    global $soronet_custom_settings;

    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    $is_valid_nonce = isset($_POST['soronet_nonce']) && wp_verify_nonce($_POST['soronet_nonce'],basename(__FILE__) . '-custom-settings') ? 'true' : 'false';
	$full_edit_mode = isset($_POST['full_edit_mode']);

    if ( $is_autosave || $is_revision || !$is_valid_nonce || !$full_edit_mode ) {
        return;
    }
    foreach($soronet_custom_settings as $key => $value){
        if($value['type'] == 'checkbox'){
            if(isset($_POST[KEY_PREFIX.$key])){
                update_post_meta($post_id,KEY_PREFIX.$key,'yes');
            } else {
                update_post_meta($post_id,KEY_PREFIX.$key,'');
            }
        }
	    if($value['type'] == 'image'){
		    if(isset($_POST[KEY_PREFIX.$key])){
			    update_post_meta($post_id,KEY_PREFIX.$key,sanitize_text_field($_POST[KEY_PREFIX.$key]));
		    }
	    }
	    if($value['type'] == 'textfield'){
		    if(isset($_POST[KEY_PREFIX.$key])){
			    update_post_meta($post_id,KEY_PREFIX.$key,sanitize_text_field($_POST[KEY_PREFIX.$key]));
		    } else {
			    update_post_meta($post_id,KEY_PREFIX.$key,'');
		    }
	    }
	    if($value['type'] == 'textarea'){
		    if(isset($_POST[KEY_PREFIX.$key])){
			    update_post_meta($post_id,KEY_PREFIX.$key,wp_filter_kses($_POST[KEY_PREFIX.$key]));
		    } else {
			    update_post_meta($post_id,KEY_PREFIX.$key,'');
		    }
	    }
	    if($value['type'] == 'editor'){
		    if(isset($_POST[KEY_PREFIX.$key])){
			    update_post_meta($post_id,KEY_PREFIX.$key,apply_filters('the_content',$_POST[KEY_PREFIX.$key]));
		    } else {
			    update_post_meta($post_id,KEY_PREFIX.$key,'');
		    }
	    }
    }
}
add_action('save_post','soronet_save_custom_settings');

function soronet_get_setting($post_id,$metabox,$key,$size = 'thumbnail'){
	global $soronet_custom_settings;
	$key = $metabox.$key;
	$type = $soronet_custom_settings[$key]['type'];
	if( $type == 'checkbox' || $type =='textfield' || $type == 'textarea' || $type == 'editor'){
        return get_post_meta($post_id,KEY_PREFIX.$key,true);
	}
	if($type =='image'){
		$img = wp_get_attachment_image_src(get_post_meta($post_id,KEY_PREFIX.$key,true),$size);
		return $img[0];
	}
}
function soronet_get_setting_name($metabox_id, $setting_id){
	global $soronet_custom_settings;
	return $soronet_custom_settings[$metabox_id.$setting_id]['name'];
}
function soronet_add_setting($metabox,$key, $name, $type,$supported_post_types = array(),$supported_templates = array()){
	global $soronet_custom_settings;
	global $supported_setting_types;
	global $screens;

	if(sizeof($supported_post_types) < 1){
		$supported_post_types = $screens;
	}
	if(in_array($type,$supported_setting_types)){
		$soronet_custom_settings[$metabox.$key] = array(
			'metabox' => $metabox,
			'name' => $name,
			'type' => $type,
			'supported_post_types' => $supported_post_types,
			'supported_templates' => $supported_templates
		);
	}
}

function soronet_display_setting($type,$key,$post_id){
    switch($type){
        case 'checkbox':
            return soronet_display_checkbox($key,$post_id);
            break;
	    case 'image':
		    return soronet_display_image($key,$post_id);
	        break;
	    case 'textfield':
		    return soronet_display_textfield($key,$post_id);
	        break;
	    case 'textarea':
		    return soronet_display_textarea($key,$post_id);
	        break;
	    case 'editor':
		    soronet_display_editor($key,$post_id);
	    break;
    }
}
function soronet_display_editor($key,$post_id){
	global $editors;
	$base_id = 'soronet_editor'; $id = $base_id;
	$content = get_post_meta($post_id,KEY_PREFIX.$key,true);

	$count = 0;
	while(in_array($id,$editors)){
		$id = $id .'_'.$count;
	}
	array_push($editors,$id);
	wp_editor($content,$id,array('textarea_name' => KEY_PREFIX.$key ));
}
function soronet_display_checkbox($key,$post_id){
    $setting = get_post_meta($post_id,KEY_PREFIX.$key,true);
    $html = '<input type="checkbox" id="'.$key.'" name="'.KEY_PREFIX.$key.'" ';
    if($setting == 'yes'){
        $html .= 'checked >';
    }
    else{
        $html .='>';
    }
    return $html;
}

function soronet_display_textfield($key,$post_id){
	$setting = get_post_meta($post_id,KEY_PREFIX.$key,true);

	$html = '<input type="text" name="'.KEY_PREFIX.$key.'"';
	$html .= empty($setting) ? '>' : 'value="'.$setting.'">';

	return $html;
}
function soronet_display_textarea($key,$post_id){
	$setting = get_post_meta($post_id,KEY_PREFIX.$key,true);

	$html = '<textarea cols="50" rows="5" name="'.KEY_PREFIX.$key.'"';
	$html .= empty($setting) ? '></textarea>' : 'value="'.$setting.'">'.$setting.'</textarea>';

	return $html;
}

function soronet_display_image($key,$post_id){
	$setting = get_post_meta($post_id,KEY_PREFIX.$key,true);

	$html = '<input type="button" id="'.$key.'-button" class="meta-image-button" value="'.__('Upload','soronet').'">';
	$html .='<input type="hidden" class="meta-image" name="'.KEY_PREFIX.$key.'"';
	if(!empty($setting)){
		$html .='value="'.$setting.'">';
	}else {
		$html .='value="">';
	}
	$html .='<img class="img-preview" ';
	if(!empty($setting)){
		$img = wp_get_attachment_image_src($setting);
		$html .='src='.$img[0].'" />';
	}else $html .= '/>';

	return $html;
}
function soronet_display_image_enqueue(){
	wp_enqueue_media();

	wp_register_script( 'image-uploader', plugin_dir_url( __FILE__ ) . '/inc/media_uploader.js', array( 'jquery' ) );
	wp_localize_script( 'image-uploader', 'meta_image',
		array(
			'title' => __( 'Choose or Upload an Image', 'soronet' ),
			'button' => __( 'Use this image', 'soronet' ),
		)
	);
	wp_enqueue_script( 'image-uploader' );
}
add_action( 'admin_enqueue_scripts', 'soronet_display_image_enqueue' );

function get_current_post_type() {
	global $post, $typenow, $current_screen;

	//we have a post so we can just get the post type from that
	if ( $post && $post->post_type )
		return $post->post_type;

	//check the global $typenow - set in admin.php
	elseif( $typenow )
		return $typenow;

	//check the global $current_screen object - set in sceen.php
	elseif( $current_screen && $current_screen->post_type )
		return $current_screen->post_type;

	//lastly check the post_type querystring
	elseif( isset( $_REQUEST['post_type'] ) )
		return sanitize_key( $_REQUEST['post_type'] );

	//we do not know the post type!
	return null;
}
function prefix_allowed_html( $allowedtags, $context ) {
	$allowedtags['iframe'] = array('width' => true,'height' => true,'src' => true,'frameborder' => true,'allowfullscreen' => true,'style' =>true);

	return $allowedtags;
}
add_filter( 'wp_kses_allowed_html', 'prefix_allowed_html', 10, 2);